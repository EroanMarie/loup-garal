package com.dev.bemeth.loupgaral;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Eroan on 23/09/2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;
    HashMap<Integer, String> listMapPseudo, listMapNumber;
    ArrayList<String> rolesList;

    public PagerAdapter(FragmentManager fm, int numOfTabs, HashMap<Integer, String> listMapPseudo, ArrayList<String> rolesList, HashMap<Integer, String> listMapNumber) {
        super(fm);
        this.numOfTabs = numOfTabs;
        this.listMapPseudo = listMapPseudo;
        this.rolesList = rolesList;
        this.listMapNumber = listMapNumber;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentPlayersRoles tab1 = new TabFragmentPlayersRoles(listMapPseudo, rolesList);
                return tab1;
            case 1:
                TabFragmentGame tab2 = new TabFragmentGame();
                return tab2;
            case 2:
                TabFragmentOptions tab3 = new TabFragmentOptions(listMapPseudo, rolesList, listMapNumber);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}