package com.dev.bemeth.loupgaral;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@SuppressLint("ValidFragment")
public class TabFragmentOptions extends Fragment implements View.OnClickListener{

    Button playAgain, mainMenu;
    HashMap<Integer, String> listMapNumber, listMapPseudo;
    List<String> rolesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View tab_options = inflater.inflate(R.layout.tab_options, container, false);
        playAgain = (Button) tab_options.findViewById(R.id.play_again);
        mainMenu = (Button) tab_options.findViewById(R.id.main_menu);
        playAgain.setOnClickListener(this);
        mainMenu.setOnClickListener(this);
        return tab_options;
    }

    @SuppressLint("ValidFragment")
    public TabFragmentOptions(HashMap<Integer, String> listMapPseudo, ArrayList<String> rolesList, HashMap<Integer, String> listMapNumber){
        this.listMapNumber = listMapNumber;
        this.listMapPseudo = listMapPseudo;
        this.rolesList = rolesList;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.play_again:
                Collections.shuffle(rolesList);
                for (int i = 0; i < listMapNumber.size(); i++) {
                    System.out.println(rolesList.get(i) + " : " + (rolesList.get(i) + " : " + getStringResByString(rolesList.get(i).replaceAll("\\s|'|-", "_"))).length());
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(listMapNumber.get(i), null, rolesList.get(i) + " : " + getStringResByString(rolesList.get(i).replaceAll("\\s|'|-", "_")), null, null);
                }
                Intent gameIntent = new Intent(getActivity(), GameActivity.class);
                gameIntent.putExtra("listMapNumber", listMapNumber);
                gameIntent.putExtra("listMapPseudo", listMapPseudo);
                gameIntent.putStringArrayListExtra("rolesList", (ArrayList<String>) rolesList);
                startActivity(gameIntent);
                break;
            case R.id.main_menu:
                Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                startActivity(mainIntent);
                break;
        }
    }

    public String getStringResByString(String key){
        String retString = key;
        int id = getResources().getIdentifier(key, "string", getActivity().getPackageName());
        if (id != 0) {
            retString = getString(id);
        }
        return retString;
    }
}
