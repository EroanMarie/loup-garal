package com.dev.bemeth.loupgaral;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eroan on 23/09/2018.
 */

public class ListAdapterPlayersRoles extends ArrayAdapter<String> {

    ArrayList roles;
    List<String> pseudos;

    private static class ViewHolder{
        public TextView pseudoTextView;
        public CheckBox loveCheckBox, deadCheckBox;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rowlayoutplayersroles, parent, false);
            viewHolder = new ViewHolder();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.pseudoTextView = (TextView) convertView.findViewById(R.id.players_pseudo);
        viewHolder.deadCheckBox = (CheckBox) convertView.findViewById(R.id.dead);
        viewHolder.loveCheckBox = (CheckBox) convertView.findViewById(R.id.love);
        viewHolder.pseudoTextView.setText(pseudos.get(position));

        return convertView;
    }

    public ListAdapterPlayersRoles(Context context, List<String> pseudos, ArrayList<String> roles) {
        super(context, R.layout.rowlayoutplayersroles, pseudos);
        this.roles = roles;
        this.pseudos = pseudos;
    }

}
