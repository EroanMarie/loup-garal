package com.dev.bemeth.loupgaral;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button startButton, howtoplayButton, supportButton;
    int players_number;
    RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, "ca-app-pub-9838952753591662/7779906301");

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {

            }

            @Override
            public void onRewardedVideoAdOpened() {

            }

            @Override
            public void onRewardedVideoStarted() {

            }

            @Override
            public void onRewardedVideoAdClosed() {
                loadRewardedVideoAd();
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {

            }

            @Override
            public void onRewardedVideoCompleted() {

            }
        });
        loadRewardedVideoAd();

        startButton = (Button) findViewById(R.id.start);
        howtoplayButton = (Button) findViewById(R.id.howto);
        supportButton = (Button) findViewById(R.id.support);
        supportButton.setOnClickListener(this);
        howtoplayButton.setOnClickListener(this);
        startButton.setOnClickListener(this);
    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-9838952753591662/7779906301",
                new AdRequest.Builder().build());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.start:
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setMessage("Entrez le nombre de joueurs");
                alert.setTitle("Joueurs");
                final NumberPicker players_number_picker = new NumberPicker(this);
                players_number_picker.setMinValue(6);
                players_number_picker.setMaxValue(30);
                alert.setView(players_number_picker);
                alert.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        players_number = players_number_picker.getValue();
                        //Log.i("Info", String.valueOf(players_number));
                        Intent intent = new Intent(MainActivity.this, RoleActivity.class);
                        intent.putExtra("players_number",players_number);
                        startActivity(intent);

                    }
                });
                alert.show();
                break;
            case R.id.howto:
                final AlertDialog.Builder alert2 = new AlertDialog.Builder(this);
                alert2.setMessage("Tout d'abord choisir le nombre de joueurs, puis choisir les rôles avec lesquels vous voulez jouer, entrez les pseudos et numéros de téléphones des joueurs et validez. Vous y êtes, la partie peut démarrer.");
                alert2.setTitle("Comment jouer ?");
                //alert.setCancelable(false);
                alert2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert2.show();
                break;
            case R.id.support:
                final AlertDialog.Builder alert3 = new AlertDialog.Builder(this);
                alert3.setMessage(getString(R.string.support));
                alert3.setTitle("Comment jouer ?");
                alert3.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                //alert.setCancelable(false);
                alert3.setPositiveButton("Regarder une pub", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (mRewardedVideoAd.isLoaded()) {
                            mRewardedVideoAd.show();
                        }else{
                            loadRewardedVideoAd();
                        }
                    }
                });
                alert3.show();
                break;
        }
    }
}
