package com.dev.bemeth.loupgaral;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Eroan on 23/09/2018.
 */

@SuppressLint("ValidFragment")
public class TabFragmentPlayersRoles extends Fragment {

    ListView listViewPlayersRoles;
    HashMap<Integer, String> listMapPseudo;
    ArrayList<String> rolesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View tab_players_roles = inflater.inflate(R.layout.tab_players_roles, container, false);
        listViewPlayersRoles = (ListView) tab_players_roles.findViewById(R.id.list_players_roles);
        final List<String> listPseudo = new ArrayList<>();
        for(int i = 0; i < listMapPseudo.size(); i++){
            listPseudo.add(listMapPseudo.get(i));
            System.out.println(listPseudo.get(i) + " - - - - " + listMapPseudo.get(i));
        }
        ListAdapterPlayersRoles adapterPlayersRoles = new ListAdapterPlayersRoles(getContext(), listPseudo, rolesList);
        listViewPlayersRoles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setMessage(rolesList.get(i) + "\n" + getStringResByString(rolesList.get(i).replaceAll("\\s|'|-", "_")));
                alert.setTitle(listPseudo.get(i));
                //alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                return ;
            }
        });
        listViewPlayersRoles.setAdapter(adapterPlayersRoles);

        return tab_players_roles;
    }

    @SuppressLint("ValidFragment")
    public TabFragmentPlayersRoles(HashMap<Integer, String> listMapPseudo, ArrayList<String> rolesList){
        this.listMapPseudo = listMapPseudo;
        this.rolesList = rolesList;
    }

    public String getStringResByString(String key){
        String retString = key;
        int id = getResources().getIdentifier(key, "string", getContext().getPackageName());
        if (id != 0) {
            retString = getString(id);
        }
        return retString;
    }

}
