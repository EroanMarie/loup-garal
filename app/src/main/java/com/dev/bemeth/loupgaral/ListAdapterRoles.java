package com.dev.bemeth.loupgaral;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.HashMap;
import java.util.Map;

public class ListAdapterRoles extends ArrayAdapter<Integer> {

    int[] min = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    String[] roles = {"Loup-Garou", "Simple Villageois", "Voyante", "Cupidon", "Chasseur", "Sorcière", "Petite-Fille", "Salvateur", "Idiot Du Village", "Bouc Émissaire", "Ancien", "Joueur De Flute", "Enfant Sauvage", "Renard", "Montreur D'Ours", "Chevalier à l'Épée Rouillée", "Ange", "Infect Père Des Loups", "Loup Blanc", "Corbeau", "Grand Méchant Loup"};

    public Map<String, Integer> listMapValues;

    private static class ViewHolder{
        public NumberPicker nP;
        public TextView tV;
        public boolean firstTime=false;

    }


    //TRES IMPORTANT POUR QUE LA LISTVIEW NE FASSE PAS UN "EFFET MIROIR" SUR LES VALEURS DES NUMBERPICKERS
    //Enlever pour voir le changement mais ne pas oublier de remettre pour ne pas avoir des problèmes
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    //JUSQUE LÀ


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rowlayoutroles, parent, false);
            viewHolder = new ViewHolder();

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //Log.d("DEBUG","passed here, so finished creating listviewroles");
        viewHolder.nP = (NumberPicker) convertView.findViewById(R.id.numberPicker);
        viewHolder.nP.setMax(getItem(position));
        viewHolder.nP.setMin(min[position]);
        System.out.println(String.valueOf(viewHolder.firstTime));
        if(viewHolder.firstTime == false){
            viewHolder.nP.setValue(viewHolder.nP.getMin());
            System.out.println("Passed Here");
            viewHolder.firstTime = true;
            System.out.println(String.valueOf(viewHolder.firstTime));
        }
        //viewHolder.nP.setValue(viewHolder.nP.getMin());
        viewHolder.nP.setValueChangedListener(new ValueChangedListener() {
            @Override
            public void valueChanged(int value, ActionEnum action) {
                //Log.d("DEBUG", "Value changed at position " + position);
                listMapValues.put(roles[position], viewHolder.nP.getValue());
            }
        });
        viewHolder.tV = (TextView) convertView.findViewById(R.id.name);
        viewHolder.tV.setText(roles[position]);
        return convertView;
    }



    public ListAdapterRoles(Context context, Integer[] values) {
        super(context, R.layout.rowlayoutroles, values);
        listMapValues = new HashMap<String, Integer>();
        listMapValues.put(roles[0], 1);
        for(int i = 1; i < getCount(); i++){
            listMapValues.put(roles[i],0);
        }

    }
}
