package com.dev.bemeth.loupgaral;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Eroan on 13/09/2018.
 */
public class RoleActivity extends AppCompatActivity implements View.OnClickListener {

    Integer[] max;
    int etape = 0;
    int players_number;
    Button nextButton, cancelButton;
    Boolean areNumbersOk = false, authorized, authorized_contacts, authorized_state;
    SmsManager smsManager;
    ListAdapterRoles adapterRoles;
    ListAdapterNumero adapterNumero;
    HashMap<Integer, String> listMapNumber, listMapPseudo;
    List<String> rolesList = new ArrayList<>();
    ListView listview;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d("DEBUG", "Went on RoleActivity");
        setContentView(R.layout.activity_role);
        nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnClickListener(this);
        cancelButton = (Button) findViewById(R.id.cancel);
        cancelButton.setOnClickListener(this);
        //Log.d("DEBUG", "setContentView done");
        listview = (ListView) findViewById(R.id.list_role);
        Intent i = getIntent();
        players_number = i.getExtras().getInt("players_number");
        max = new Integer[]{(int) Math.floor(players_number / 2.5), players_number - 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        //Log.d("DEBUG", "Before creating adapter");
        adapterRoles = new ListAdapterRoles(this, max);
        listview.setAdapter(adapterRoles);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next:
                if (etape == 0) {
                    int total = 0;
                    for(Map.Entry<String, Integer> entry : adapterRoles.listMapValues.entrySet()){
                        System.out.println(entry.getKey() + " - " + entry.getValue());
                        total += entry.getValue();
                        for(int i = 0; i < entry.getValue(); i++){
                            rolesList.add(entry.getKey());
                        }
                    }
                    if (total < players_number) {
                        Toast.makeText(this, "Il manque " + (players_number - total) + " rôle(s)", Toast.LENGTH_SHORT).show();
                        rolesList.clear();
                    } else if (total > players_number) {
                        Toast.makeText(this, "Il y a " + (total - players_number) + " rôle(s) en trop", Toast.LENGTH_SHORT).show();
                        rolesList.clear();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                                authorized_contacts = false;
                                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 2);
                            }else{
                                authorized_contacts = true;
                            }
                            if (checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                                authorized = false;
                                requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 1);
                            }else{
                                authorized = true;
                            }
                            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                authorized_state = false;
                                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 3);
                            }else{
                                authorized_state = true;
                            }
                        }
                        if (authorized_contacts && authorized && authorized_state) {

                            Log.d("Debug", "C'est good ça");
                            etape++;
                            adapterNumero = new ListAdapterNumero(this, rolesList.toArray());
                         /*   ViewGroup.LayoutParams params = listview.getLayoutParams();
                            params.height = getResources().getDimensionPixelSize(R.dimen.listnumber);
                        for(int i = 0; i < rolesList.size(); i++){
                            System.out.println(rolesList.get(i) + " : " + getStringResByString(rolesList.get(i).replaceAll("\\s|'|-", "_")).length());
                        }
                            listview.setLayoutParams(params);*/
                            listview.setAdapter(adapterNumero);
                        }
                    }
                }else if(etape==1){
                    if(adapterNumero.listMapNumber.size() != players_number || adapterNumero.listMapPseudo.size() != players_number){
                            Toast.makeText(this, "Il manque des informations", Toast.LENGTH_SHORT).show();
                    }else {
                        for (int i = 0; i < adapterNumero.listMapPseudo.size(); i++) {
                            if (adapterNumero.listMapPseudo.get(i).equals("") && adapterNumero.listMapNumber.get(i).equals("")) {
                                Toast.makeText(this, "Il manque des informations", Toast.LENGTH_SHORT).show();
                                areNumbersOk = false;
                            }else{
                                areNumbersOk = true;
                            }
                        }
                        if(areNumbersOk) {
                            if(authorized && authorized_state) {
                                listMapNumber = adapterNumero.listMapNumber;
                                listMapPseudo = adapterNumero.listMapPseudo;

                                Collections.shuffle(rolesList);
                                for (int i = 0; i < players_number; i++) {
                                    System.out.println(rolesList.get(i) + " : " + (rolesList.get(i) + " : " + getStringResByString(rolesList.get(i).replaceAll("\\s|'|-", "_"))).length());
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage(listMapNumber.get(i), null, rolesList.get(i) + " : " + getStringResByString(rolesList.get(i).replaceAll("\\s|'|-", "_")), null, null);
                                }

                                Intent gameIntent = new Intent(RoleActivity.this, GameActivity.class);
                                gameIntent.putExtra("listMapNumber", listMapNumber);
                                gameIntent.putExtra("listMapPseudo", listMapPseudo);
                                gameIntent.putStringArrayListExtra("rolesList", (ArrayList<String>) rolesList);
                                startActivity(gameIntent);
                            }else{

                            }
                        }
                    }
                }
                break;
            case R.id.cancel:
                if(etape==0){
                    Intent cancelIntent = new Intent(RoleActivity.this, MainActivity.class);
                    startActivity(cancelIntent);
                }else if (etape==1){
                    etape=0;
                  /*ViewGroup.LayoutParams params = listview.getLayoutParams();
                    params.height = getResources().getDimensionPixelSize(R.dimen.listroles);
                    listview.setLayoutParams(params);*/
                    adapterRoles = new ListAdapterRoles(this, max);
                    listview.setAdapter(adapterRoles);
                }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    authorized = true;
                } else{
                    Toast.makeText(this, "Vous devez donner la permission à l'application d'envoyer des SMS", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    authorized_contacts = true;
                } else{
                    Toast.makeText(this, "Vous devez donner la permission à l'application d'envoyer de lire vos contacts", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 3: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    authorized_state = true;
                } else{
                    Toast.makeText(this, "Vous devez donner la permission à l'application d'envoyer de lire l'état du téléphone", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public String getStringResByString(String key){
        String retString = key;
        int id = getResources().getIdentifier(key, "string", getPackageName());
        if (id != 0) {
            retString = getString(id);
        }
        return retString;
    }



}
