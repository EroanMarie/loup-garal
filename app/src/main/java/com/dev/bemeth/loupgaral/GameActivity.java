package com.dev.bemeth.loupgaral;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Eroan on 22/09/2018.
 */

public class GameActivity extends AppCompatActivity {

    ListView listViewPlayersRoles;
    HashMap<Integer, String> listMapPseudo, listMapNumber;
    ArrayList<String> rolesList;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Dans le cas où certaines personnes n'auraient pas reçu le message avec leur rôle, vous pouvez toujours leurs montrer en cliquant sur leur nom");
        alert.setTitle("INFORMATION");
        //alert.setCancelable(false);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
        Intent intent = getIntent();
        listMapPseudo = (HashMap<Integer, String>) intent.getExtras().get("listMapPseudo");
        listMapNumber = (HashMap<Integer, String>) intent.getExtras().get("listMapNumber");
        rolesList = intent.getStringArrayListExtra("rolesList");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Rôles"));
        tabLayout.addTab(tabLayout.newTab().setText("Déroulement"));
        tabLayout.addTab(tabLayout.newTab().setText("Options"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(), listMapPseudo, rolesList, listMapNumber);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
