package com.dev.bemeth.loupgaral;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eroan on 14/09/2018.
 */

public class ListAdapterNumero extends ArrayAdapter<Object> {

    private static class ViewHolder{
        public AutoCompleteTextView number, pseudo;
        public TextView positionTV;
    }

    private ArrayList<Map<String, String>> arrayListMap;
    private SimpleAdapter simpleAdapter;
    public HashMap<Integer, String> listMapPseudo, listMapNumber;

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rowlayoutnumber, parent, false);
            viewHolder = new ViewHolder();

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.number = (AutoCompleteTextView) convertView.findViewById(R.id.number);
        viewHolder.pseudo = (AutoCompleteTextView) convertView.findViewById(R.id.pseudo);
        viewHolder.positionTV = (TextView) convertView.findViewById(R.id.positionTV);


/*
        viewHolder.number.setText("06 93 33 09 26");
        viewHolder.pseudo.setText("Moi" + (position + 1));

*/
        //viewHolder.pseudo.setDropDownVerticalOffset((-1*viewHolder.pseudo.getHeight()));
        viewHolder.positionTV.setText((position + 1) + " :");
        viewHolder.pseudo.setDropDownAnchor(R.id.number);
        viewHolder.pseudo.setThreshold(1);
        viewHolder.number.setThreshold(1);
        simpleAdapter = new SimpleAdapter(getContext(), arrayListMap, R.layout.phonedropdownlayout,
                new String[] { "Name", "Phone"}, new int[] {
                R.id.dropName, R.id.dropNumber});
        viewHolder.pseudo.setAdapter(simpleAdapter);
        viewHolder.pseudo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                System.out.println("Pseudo focus changed on position : "  + position);
                listMapPseudo.put(position, String.valueOf(viewHolder.pseudo.getText()));
                listMapNumber.put(position, String.valueOf(viewHolder.number.getText()));
                System.out.println(listMapNumber.get(position));
                System.out.println(listMapPseudo.get(position));

            }
        });
        viewHolder.number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                System.out.println("Number focus changed on position : "  + position);
                listMapPseudo.put(position, String.valueOf(viewHolder.pseudo.getText()));
                listMapNumber.put(position, String.valueOf(viewHolder.number.getText()));
                System.out.println(listMapNumber.get(position));
                System.out.println(listMapPseudo.get(position));
            }
        });
        viewHolder.pseudo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Map<String, String> map = (Map<String, String>) adapterView.getItemAtPosition(i);

                String name  = map.get("Name");
                String number = map.get("Phone");
                viewHolder.number.setText(number);
                viewHolder.pseudo.setText(name);
            }
        });


        //Log.d("DEBUG","passed here, so finished creating listviewnumero");
        return convertView;
    }

    public ListAdapterNumero(Context context,  Object[] objects) {
        super(context,R.layout.rowlayoutnumber, objects);
        listMapPseudo = new HashMap<>();
        listMapNumber = new HashMap<>();
        arrayListMap = new ArrayList<>();
        PopulatePeopleList();
    }

    public void PopulatePeopleList() {
        arrayListMap.clear();
        Cursor people = getContext().getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while (people.moveToNext()) {
            String contactName = people.getString(people
                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            String contactId = people.getString(people
                    .getColumnIndex(ContactsContract.Contacts._ID));
            String hasPhone = people
                    .getString(people
                            .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

            if ((Integer.parseInt(hasPhone) > 0)){
                Cursor phones = getContext().getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId,
                        null, null);
                while (phones.moveToNext()){
                    String phoneNumber = phones.getString(
                            phones.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String numberType = phones.getString(phones.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.TYPE));
                    Map<String, String> NamePhoneType = new HashMap<String, String>();
                    NamePhoneType.put("Name", contactName);
                    NamePhoneType.put("Phone", phoneNumber);
                    if(numberType.equals("0"))
                        NamePhoneType.put("Type", "Work");
                    else
                    if(numberType.equals("1"))
                        NamePhoneType.put("Type", "Home");
                    else if(numberType.equals("2"))
                        NamePhoneType.put("Type",  "Mobile");
                    else
                        NamePhoneType.put("Type", "Other");
                    arrayListMap.add(NamePhoneType);
                }
                phones.close();
            }
        }
        people.close();
    }
}
