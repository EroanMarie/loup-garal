package com.dev.bemeth.loupgaral;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Eroan on 23/09/2018.
 */

public class TabFragmentGame extends Fragment {

    List<String> prepNight = new ArrayList<>();
    List<String> otherNights = new ArrayList<>();
    List<String> day = new ArrayList<>();
    List<String> deathEffects = new ArrayList<>();
    ExpandableListView expandableListView;
    CustomExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    LinkedHashMap<String, List<String>> expandableListDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        prepNight.add(getString(R.string.nuit));
        prepNight.add(getString(R.string.appelCupidon));
        prepNight.add(getString(R.string.appelAmoureux));
        prepNight.add(getString(R.string.appelEnfantSauvage));
        otherNights.add(getString(R.string.appelVoyante));
        otherNights.add(getString(R.string.appelSalvateur));
        otherNights.add(getString(R.string.appelLoupsGarous));
        otherNights.add(getString(R.string.appelInfectPèreDesLoups));
        otherNights.add(getString(R.string.appelGrandMéchantLoup));
        otherNights.add(getString(R.string.appelLoupBlanc));
        otherNights.add(getString(R.string.appelJoueurDeFlûte));
        otherNights.add(getString(R.string.appelRenard));
        otherNights.add(getString(R.string.appelCorbeau));
        otherNights.add(getString(R.string.appelSorcière));
        day.add(getString(R.string.jour));
        day.add(getString(R.string.effetMontreurDOurs));
        day.add(getString(R.string.election));
        day.add(getString(R.string.bucher));
        day.add(getString(R.string.nuit));
        deathEffects.add(getString(R.string.mortAmoureux));
        deathEffects.add(getString(R.string.mortAncien));
        deathEffects.add(getString(R.string.mortAnge));
        deathEffects.add(getString(R.string.mortChasseur));
        deathEffects.add(getString(R.string.mortChevalierÀLÉpéeRouillée));
        deathEffects.add(getString(R.string.mortIdiotDuVillage));
        deathEffects.add(getString(R.string.mortModèle));
        View view = inflater.inflate(R.layout.tab_game, container, false);
        expandableListView = (ExpandableListView) view.findViewById(R.id.expendableListView);
        expandableListDetail = getData(day, prepNight, otherNights, deathEffects);
        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getContext(), expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);

        return view;
    }

    public static LinkedHashMap<String, List<String>> getData(List<String> day, List<String> prepNight, List<String> otherNights, List<String> deathEffects) {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();

        expandableListDetail.put("Nuit de préparation", prepNight);
        expandableListDetail.put("Les autres nuits", otherNights);
        expandableListDetail.put("Jour", day);
        expandableListDetail.put("Effets à la mort", deathEffects);
        return expandableListDetail;
    }

}
