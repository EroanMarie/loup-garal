# Loup-Garal

Règles de confidentialité suite à la permission de lire les contacts dans l'application

FR-fr : La permission pour lire les contacts sert uniquement à faire de l'autocomplétion dans la partie contacts, je n'enregistre aucune information sur les contacts
EN-en : The permission to read contacts is only used to autocomplete edit texts. I don't save contacts information. 